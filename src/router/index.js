import { createRouter, createWebHashHistory } from 'vue-router'
import Login from '../views/Login.vue'

const routes = [
  {
    path: '/',
    redirect: 'login'
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/home',
    name: 'Home',
    component: () => import('../views/Home.vue'),
    redirect: 'welcome',
    children:[
      {
        path: '/welcome',
        name: 'Welcome',
        component:()=>import('../views/Welcome')
      },
      {
        path: '/users',
        name: 'Users',
        component:()=>import('../views/user/Users')
      },
      {
        path: '/rights',
        name: 'Rights',
        component:()=>import('../views/power/Rights')
      },
      {
        path: '/roles',
        name: 'Roles',
        component:()=>import('../views/power/Roles')
      },
      {
        path: '/cate',
        name: 'Cate',
        component:()=>import('../views/goods/Cate')
      },
      {
        path: '/list',
        name: 'List',
        component:()=>import('../views/goods/List')
      },
      {
        path: '/list/add',
        name: 'Add',
        component:()=>import('../views/goods/Add')
      },
      {
        path: '/list/edit',
        name: 'Edit',
        component:()=>import('../views/goods/Edit')
      },
      {
        path: '/report',
        name: 'Report',
        component:()=>import('../views/report/Report')
      }
    ]
  }
]



const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  if (to.path === '/login') {
    return next()
  }
  const token = window.sessionStorage.getItem('token')
  if (!token) {
    return next('/login')
  }
  next()
})

export default router
