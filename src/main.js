import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import "element-plus/dist/index.css";
import { components } from "./element-plus/element.ts";
import './assets/css/global.css'

const app=createApp(App);
for (const cpn of components) {
    app.component(cpn.name, cpn);
  }
app.use(store).use(router).mount('#app')
