import { createStore } from 'vuex'
import axios from 'axios'

export default createStore({
  state: {
    
    userinfo: {
      username: '',
      name: '',
      token:''
    },
    durginfo:{
      drugId:''
    }

  },
  mutations: {
    LOGIN(state,info) {
        state.userinfo.username=info.username
        state.userinfo.name=info.name
        state.userinfo.token=info.userId
    },
    editDrugId(state,id){
      state.durginfo.drugId=id
    }
  },
  actions: {
    login(context,info) {
      axios.post('http://localhost:8081/login',info).then(resp=>{
        // console.log(resp)
        if(resp.data.status!=200){
          return ;
        }
        window.sessionStorage.setItem('token',resp.data.data.userId)
        context.commit("LOGIN",resp.data.data)
      })
    }
  },
  modules: {
  }
})
